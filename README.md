# Details

This repository contains the files necessary to reproduce the DFT simulations contained in the : *Control of spintronic and electronic properties of bimetallic and vacancy-ordered vanadium carbide MXenes via surface functionalization* published in  Physical Chemistry Chemical Physics.

Preprint available on arXiv https://arxiv.org/abs/1910.01509

DOI: 10.1039/c9cp05638f.


